#pragma once

#include <string>
#include <vector>

void hex2bin(std::string& input, std::vector<unsigned char>& output, size_t position);
void hex2bin(std::string& input, std::vector<unsigned int>& output, size_t position);
void hex2bin_reverse(std::string& input, size_t start, size_t length, std::vector<unsigned char>& output, size_t position);
std::string bin2hex(std::vector<unsigned char> input, size_t start, size_t length);
std::string bin2hex_reverse(std::vector<unsigned char> input, size_t start, size_t length);
