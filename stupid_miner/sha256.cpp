#include "sha256.h"

using namespace std;

static const unsigned int K256[64] =
{
    0x428a2f98UL, 0x71374491UL, 0xb5c0fbcfUL, 0xe9b5dba5UL,
    0x3956c25bUL, 0x59f111f1UL, 0x923f82a4UL, 0xab1c5ed5UL,
    0xd807aa98UL, 0x12835b01UL, 0x243185beUL, 0x550c7dc3UL,
    0x72be5d74UL, 0x80deb1feUL, 0x9bdc06a7UL, 0xc19bf174UL,
    0xe49b69c1UL, 0xefbe4786UL, 0x0fc19dc6UL, 0x240ca1ccUL,
    0x2de92c6fUL, 0x4a7484aaUL, 0x5cb0a9dcUL, 0x76f988daUL,
    0x983e5152UL, 0xa831c66dUL, 0xb00327c8UL, 0xbf597fc7UL,
    0xc6e00bf3UL, 0xd5a79147UL, 0x06ca6351UL, 0x14292967UL,
    0x27b70a85UL, 0x2e1b2138UL, 0x4d2c6dfcUL, 0x53380d13UL,
    0x650a7354UL, 0x766a0abbUL, 0x81c2c92eUL, 0x92722c85UL,
    0xa2bfe8a1UL, 0xa81a664bUL, 0xc24b8b70UL, 0xc76c51a3UL,
    0xd192e819UL, 0xd6990624UL, 0xf40e3585UL, 0x106aa070UL,
    0x19a4c116UL, 0x1e376c08UL, 0x2748774cUL, 0x34b0bcb5UL,
    0x391c0cb3UL, 0x4ed8aa4aUL, 0x5b9cca4fUL, 0x682e6ff3UL,
    0x748f82eeUL, 0x78a5636fUL, 0x84c87814UL, 0x8cc70208UL,
    0x90befffaUL, 0xa4506cebUL, 0xbef9a3f7UL, 0xc67178f2UL
};

static const unsigned int Start_Values[16] =
{
    0x6a09e667UL, 0xbb67ae85UL, 0x3c6ef372UL, 0xa54ff53aUL,
    0x510e527fUL, 0x9b05688cUL, 0x1f83d9abUL, 0x5be0cd19UL,
    0x80000000UL, 0x00000000UL, 0x00000000UL, 0x00000000UL,
    0x00000000UL, 0x00000000UL, 0x00000000UL, 0x00000100UL
};

#define ROTATE(a,n) (((a)<<(n))|(((a)&0xffffffff)>>(32-(n))))

#define Sigma0(x) (ROTATE((x),30) ^ ROTATE((x),19) ^ ROTATE((x),10))
#define Sigma1(x) (ROTATE((x),26) ^ ROTATE((x),21) ^ ROTATE((x),7))
#define sigma0(x) (ROTATE((x),25) ^ ROTATE((x),14) ^ ((x)>>3))
#define sigma1(x) (ROTATE((x),15) ^ ROTATE((x),13) ^ ((x)>>10))

#define Ch(x,y,z) (((x) & (y)) ^ ((~(x)) & (z)))
#define Maj(x,y,z) (((x) & (y)) ^ ((x) & (z)) ^ ((y) & (z)))

void SHA256_Twice(std::vector<unsigned char>& input, size_t length, std::vector<unsigned int>& output)
{
    auto data = input.data();
    auto Nl = (unsigned int)((length << 3) & 0xFFFFFFFFUL);
    auto Nh = (unsigned int)(length >> 29);
    data[length] = 0x80;
    auto at_end = input.size() - 8;
    data[at_end++] = (Nh >> 24) & 0xFF;
    data[at_end++] = (Nh >> 16) & 0xFF;
    data[at_end++] = (Nh >> 8) & 0xFF;
    data[at_end++] = Nh & 0xFF;
    data[at_end++] = (Nl >> 24) & 0xFF;
    data[at_end++] = (Nl >> 16) & 0xFF;
    data[at_end++] = (Nl >> 8) & 0xFF;
    data[at_end++] = Nl & 0xFF;
    unsigned int intermediate[16];
    copy(Start_Values, Start_Values + 16, intermediate);
    unsigned int X[16];
    auto num = (unsigned int)(at_end / 64);
    while (num > 0)
    {
        auto a = intermediate[0];
        auto b = intermediate[1];
        auto c = intermediate[2];
        auto d = intermediate[3];
        auto e = intermediate[4];
        auto f = intermediate[5];
        auto g = intermediate[6];
        auto h = intermediate[7];
        auto i = 0;
        for (; i < 16; i++)
        {
            auto l = (((unsigned int)(*(data++))) << 24);
            l |= (((unsigned int)(*(data++))) << 16);
            l |= (((unsigned int)(*(data++))) << 8);
            l |= ((unsigned int)(*(data++)));
            auto T1 = X[i] = l;
            T1 += h + Sigma1(e) + Ch(e, f, g) + K256[i];
            auto T2 = Sigma0(a) + Maj(a, b, c);
            h = g;
            g = f;
            f = e;
            e = d + T1;
            d = c;
            c = b;
            b = a;
            a = T1 + T2;
        }
        for (; i < 64; i++)
        {
            auto s0 = X[(i + 1) & 0x0f];
            s0 = sigma0(s0);
            auto s1 = X[(i + 14) & 0x0f];
            s1 = sigma1(s1);
            auto T1 = X[i & 0xf] += s0 + s1 + X[(i + 9) & 0xf];
            T1 += h + Sigma1(e) + Ch(e, f, g) + K256[i];
            auto T2 = Sigma0(a) + Maj(a, b, c);
            h = g;
            g = f;
            f = e;
            e = d + T1;
            d = c;
            c = b;
            b = a;
            a = T1 + T2;
        }
        intermediate[0] += a;
        intermediate[1] += b;
        intermediate[2] += c;
        intermediate[3] += d;
        intermediate[4] += e;
        intermediate[5] += f;
        intermediate[6] += g;
        intermediate[7] += h;
        num--;
    }
    auto a = Start_Values[0];
    auto b = Start_Values[1];
    auto c = Start_Values[2];
    auto d = Start_Values[3];
    auto e = Start_Values[4];
    auto f = Start_Values[5];
    auto g = Start_Values[6];
    auto h = Start_Values[7];
    auto i = 0;
    for (; i < 16; i++)
    {
        auto T1 = X[i] = intermediate[i];
        T1 += h + Sigma1(e) + Ch(e, f, g) + K256[i];
        auto T2 = Sigma0(a) + Maj(a, b, c);
        h = g;
        g = f;
        f = e;
        e = d + T1;
        d = c;
        c = b;
        b = a;
        a = T1 + T2;
    }
    for (; i < 64; i++)
    {
        auto s0 = X[(i + 1) & 0x0f];
        s0 = sigma0(s0);
        auto s1 = X[(i + 14) & 0x0f];
        s1 = sigma1(s1);
        auto T1 = X[i & 0xf] += s0 + s1 + X[(i + 9) & 0xf];
        T1 += h + Sigma1(e) + Ch(e, f, g) + K256[i];
        auto T2 = Sigma0(a) + Maj(a, b, c);
        h = g;
        g = f;
        f = e;
        e = d + T1;
        d = c;
        c = b;
        b = a;
        a = T1 + T2;
    }
    output[0] = Start_Values[0] + a;
    output[1] = Start_Values[1] + b;
    output[2] = Start_Values[2] + c;
    output[3] = Start_Values[3] + d;
    output[4] = Start_Values[4] + e;
    output[5] = Start_Values[5] + f;
    output[6] = Start_Values[6] + g;
    output[7] = Start_Values[7] + h;
}

void SHA256_Twice(std::vector<unsigned int>& input, size_t length, std::vector<unsigned int>& output)
{
    auto data = input.data();
    auto Nl = (unsigned int)((length << 3) & 0xFFFFFFFFUL);
    auto Nh = (unsigned int)(length >> 29);
    data[length / 4] = 0x80000000UL;
    auto at_end = input.size() - 2;
    data[at_end++] = Nh;
    data[at_end++] = Nl;
    unsigned int intermediate[16];
    copy(Start_Values, Start_Values + 16, intermediate);
    unsigned int X[16];
    auto num = (unsigned int)(at_end / 16);
    while (num > 0)
    {
        auto a = intermediate[0];
        auto b = intermediate[1];
        auto c = intermediate[2];
        auto d = intermediate[3];
        auto e = intermediate[4];
        auto f = intermediate[5];
        auto g = intermediate[6];
        auto h = intermediate[7];
        auto i = 0;
        for (; i < 16; i++)
        {
            auto T1 = X[i] = *(data++);
            T1 += h + Sigma1(e) + Ch(e, f, g) + K256[i];
            auto T2 = Sigma0(a) + Maj(a, b, c);
            h = g;
            g = f;
            f = e;
            e = d + T1;
            d = c;
            c = b;
            b = a;
            a = T1 + T2;
        }
        for (; i < 64; i++)
        {
            auto s0 = X[(i + 1) & 0x0f];
            s0 = sigma0(s0);
            auto s1 = X[(i + 14) & 0x0f];
            s1 = sigma1(s1);
            auto T1 = X[i & 0xf] += s0 + s1 + X[(i + 9) & 0xf];
            T1 += h + Sigma1(e) + Ch(e, f, g) + K256[i];
            auto T2 = Sigma0(a) + Maj(a, b, c);
            h = g;
            g = f;
            f = e;
            e = d + T1;
            d = c;
            c = b;
            b = a;
            a = T1 + T2;
        }
        intermediate[0] += a;
        intermediate[1] += b;
        intermediate[2] += c;
        intermediate[3] += d;
        intermediate[4] += e;
        intermediate[5] += f;
        intermediate[6] += g;
        intermediate[7] += h;
        num--;
    }
    auto a = Start_Values[0];
    auto b = Start_Values[1];
    auto c = Start_Values[2];
    auto d = Start_Values[3];
    auto e = Start_Values[4];
    auto f = Start_Values[5];
    auto g = Start_Values[6];
    auto h = Start_Values[7];
    auto i = 0;
    for (; i < 16; i++)
    {
        auto T1 = X[i] = intermediate[i];
        T1 += h + Sigma1(e) + Ch(e, f, g) + K256[i];
        auto T2 = Sigma0(a) + Maj(a, b, c);
        h = g;
        g = f;
        f = e;
        e = d + T1;
        d = c;
        c = b;
        b = a;
        a = T1 + T2;
    }
    for (; i < 64; i++)
    {
        auto s0 = X[(i + 1) & 0x0f];
        s0 = sigma0(s0);
        auto s1 = X[(i + 14) & 0x0f];
        s1 = sigma1(s1);
        auto T1 = X[i & 0xf] += s0 + s1 + X[(i + 9) & 0xf];
        T1 += h + Sigma1(e) + Ch(e, f, g) + K256[i];
        auto T2 = Sigma0(a) + Maj(a, b, c);
        h = g;
        g = f;
        f = e;
        e = d + T1;
        d = c;
        c = b;
        b = a;
        a = T1 + T2;
    }
    output[0] = Start_Values[0] + a;
    output[1] = Start_Values[1] + b;
    output[2] = Start_Values[2] + c;
    output[3] = Start_Values[3] + d;
    output[4] = Start_Values[4] + e;
    output[5] = Start_Values[5] + f;
    output[6] = Start_Values[6] + g;
    output[7] = Start_Values[7] + h;
}

void SHA256_Twice(std::vector<unsigned char>& input, size_t length, std::vector<unsigned char>& output)
{
    auto data = input.data();
    auto Nl = (unsigned int)((length << 3) & 0xFFFFFFFFUL);
    auto Nh = (unsigned int)(length >> 29);
    data[length] = 0x80;
    auto at_end = input.size() - 8;
    data[at_end++] = (Nh >> 24) & 0xFF;
    data[at_end++] = (Nh >> 16) & 0xFF;
    data[at_end++] = (Nh >> 8) & 0xFF;
    data[at_end++] = Nh & 0xFF;
    data[at_end++] = (Nl >> 24) & 0xFF;
    data[at_end++] = (Nl >> 16) & 0xFF;
    data[at_end++] = (Nl >> 8) & 0xFF;
    data[at_end++] = Nl & 0xFF;
    unsigned int intermediate[16];
    copy(Start_Values, Start_Values + 16, intermediate);
    unsigned int X[16];
    auto num = (unsigned int)(at_end / 64);
    while (num > 0)
    {
        auto a = intermediate[0];
        auto b = intermediate[1];
        auto c = intermediate[2];
        auto d = intermediate[3];
        auto e = intermediate[4];
        auto f = intermediate[5];
        auto g = intermediate[6];
        auto h = intermediate[7];
        auto i = 0;
        for (; i < 16; i++)
        {
            auto l = (((unsigned int)(*(data++))) << 24);
            l |= (((unsigned int)(*(data++))) << 16);
            l |= (((unsigned int)(*(data++))) << 8);
            l |= ((unsigned int)(*(data++)));
            auto T1 = X[i] = l;
            T1 += h + Sigma1(e) + Ch(e, f, g) + K256[i];
            auto T2 = Sigma0(a) + Maj(a, b, c);
            h = g;
            g = f;
            f = e;
            e = d + T1;
            d = c;
            c = b;
            b = a;
            a = T1 + T2;
        }
        for (; i < 64; i++)
        {
            auto s0 = X[(i + 1) & 0x0f];
            s0 = sigma0(s0);
            auto s1 = X[(i + 14) & 0x0f];
            s1 = sigma1(s1);
            auto T1 = X[i & 0xf] += s0 + s1 + X[(i + 9) & 0xf];
            T1 += h + Sigma1(e) + Ch(e, f, g) + K256[i];
            auto T2 = Sigma0(a) + Maj(a, b, c);
            h = g;
            g = f;
            f = e;
            e = d + T1;
            d = c;
            c = b;
            b = a;
            a = T1 + T2;
        }
        intermediate[0] += a;
        intermediate[1] += b;
        intermediate[2] += c;
        intermediate[3] += d;
        intermediate[4] += e;
        intermediate[5] += f;
        intermediate[6] += g;
        intermediate[7] += h;
        num--;
    }
    auto a = Start_Values[0];
    auto b = Start_Values[1];
    auto c = Start_Values[2];
    auto d = Start_Values[3];
    auto e = Start_Values[4];
    auto f = Start_Values[5];
    auto g = Start_Values[6];
    auto h = Start_Values[7];
    auto i = 0;
    for (; i < 16; i++)
    {
        auto T1 = X[i] = intermediate[i];
        T1 += h + Sigma1(e) + Ch(e, f, g) + K256[i];
        auto T2 = Sigma0(a) + Maj(a, b, c);
        h = g;
        g = f;
        f = e;
        e = d + T1;
        d = c;
        c = b;
        b = a;
        a = T1 + T2;
    }
    for (; i < 64; i++)
    {
        auto s0 = X[(i + 1) & 0x0f];
        s0 = sigma0(s0);
        auto s1 = X[(i + 14) & 0x0f];
        s1 = sigma1(s1);
        auto T1 = X[i & 0xf] += s0 + s1 + X[(i + 9) & 0xf];
        T1 += h + Sigma1(e) + Ch(e, f, g) + K256[i];
        auto T2 = Sigma0(a) + Maj(a, b, c);
        h = g;
        g = f;
        f = e;
        e = d + T1;
        d = c;
        c = b;
        b = a;
        a = T1 + T2;
    }
    auto position = 0;
    auto value = Start_Values[0] + a;
    output[position++] = (value >> 24) & 0xFF;
    output[position++] = (value >> 16) & 0xFF;
    output[position++] = (value >> 8) & 0xFF;
    output[position++] = value & 0xFF;
    value = Start_Values[1] + b;
    output[position++] = (value >> 24) & 0xFF;
    output[position++] = (value >> 16) & 0xFF;
    output[position++] = (value >> 8) & 0xFF;
    output[position++] = value & 0xFF;
    value = Start_Values[2] + c;
    output[position++] = (value >> 24) & 0xFF;
    output[position++] = (value >> 16) & 0xFF;
    output[position++] = (value >> 8) & 0xFF;
    output[position++] = value & 0xFF;
    value = Start_Values[3] + d;
    output[position++] = (value >> 24) & 0xFF;
    output[position++] = (value >> 16) & 0xFF;
    output[position++] = (value >> 8) & 0xFF;
    output[position++] = value & 0xFF;
    value = Start_Values[4] + e;
    output[position++] = (value >> 24) & 0xFF;
    output[position++] = (value >> 16) & 0xFF;
    output[position++] = (value >> 8) & 0xFF;
    output[position++] = value & 0xFF;
    value = Start_Values[5] + f;
    output[position++] = (value >> 24) & 0xFF;
    output[position++] = (value >> 16) & 0xFF;
    output[position++] = (value >> 8) & 0xFF;
    output[position++] = value & 0xFF;
    value = Start_Values[6] + g;
    output[position++] = (value >> 24) & 0xFF;
    output[position++] = (value >> 16) & 0xFF;
    output[position++] = (value >> 8) & 0xFF;
    output[position++] = value & 0xFF;
    value = Start_Values[7] + h;
    output[position++] = (value >> 24) & 0xFF;
    output[position++] = (value >> 16) & 0xFF;
    output[position++] = (value >> 8) & 0xFF;
    output[position++] = value & 0xFF;
}
