#pragma once

#include "json_instance_type.h"
#include <string>
#include <unordered_map>
#include <vector>

struct json_instance
{
    json_instance_type type = json_null;
    std::string string_value;
    double numeric_value = 0;
    bool bool_value = false;
    std::unordered_map<std::string, json_instance> object_value;
    std::vector<json_instance> array_value;
    void clear();
    size_t parse(const std::vector<unsigned char>& source, size_t start);
};
