#pragma once

#include <vector>

void SHA256_Twice(std::vector<unsigned char>& input, size_t length, std::vector<unsigned int>& output);

void SHA256_Twice(std::vector<unsigned int>& input, size_t length, std::vector<unsigned int>& output);

void SHA256_Twice(std::vector<unsigned char>& input, size_t length, std::vector<unsigned char>& output);
