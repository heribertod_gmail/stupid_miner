#include "hexbin.h"

using namespace std;

void hex2bin(string& input, vector<unsigned char>& output, size_t position)
{
    unsigned char b = 0;
    auto step = 0;
    for (size_t i = 0; i < input.size(); i++)
    {
        auto c = input[i];
        switch (step)
        {
            case 0:
                if (c >= '0' && c <= '9')
                {
                    b = c - '0';
                }
                else if (c >= 'A' && c <= 'F')
                {
                    b = c + 10 - 'A';
                }
                else if (c >= 'a' && c <= 'f')
                {
                    b = c + 10 - 'a';
                }
                else
                {
                    throw runtime_error(string("Unrecognized character ") + string(1, c) + " at position " + to_string(i));
                }
                step = 1;
                break;
            case 1:
                if (c >= '0' && c <= '9')
                {
                    b = b * 16 + c - '0';
                    output[position] = b;
                    position++;
                }
                else if (c >= 'A' && c <= 'F')
                {
                    b = b * 16 + c + 10 - 'A';
                    output[position] = b;
                    position++;
                }
                else if (c >= 'a' && c <= 'f')
                {
                    b = b * 16 + c + 10 - 'a';
                    output[position] = b;
                    position++;
                }
                else
                {
                    throw runtime_error(string("Unrecognized character ") + string(1, c) + " at position " + to_string(i));
                }
                step = 0;
                break;
        }
    }
}

void hex2bin(string& input, vector<unsigned int>& output, size_t position)
{
    unsigned char b = 0;
    unsigned int v = 0;
    size_t bit_mask = 24;
    auto step = 0;
    for (size_t i = 0; i < input.size(); i++)
    {
        auto c = input[i];
        switch (step)
        {
            case 0:
                if (c >= '0' && c <= '9')
                {
                    b = c - '0';
                }
                else if (c >= 'A' && c <= 'F')
                {
                    b = c + 10 - 'A';
                }
                else if (c >= 'a' && c <= 'f')
                {
                    b = c + 10 - 'a';
                }
                else
                {
                    throw runtime_error(string("Unrecognized character ") + string(1, c) + " at position " + to_string(i));
                }
                step = 1;
                break;
            case 1:
                if (c >= '0' && c <= '9')
                {
                    b = b * 16 + c - '0';
                    v |= (unsigned int)b << bit_mask;
                    if (bit_mask == 0)
                    {
                        output[position++] = v;
                        v = 0;
                        bit_mask = 24;
                    }
                    else
                    {
                        bit_mask -= 8;
                    }
                }
                else if (c >= 'A' && c <= 'F')
                {
                    b = b * 16 + c + 10 - 'A';
                    v |= (unsigned int)b << bit_mask;
                    if (bit_mask == 0)
                    {
                        output[position++] = v;
                        v = 0;
                        bit_mask = 24;
                    }
                    else
                    {
                        bit_mask -= 8;
                    }
                }
                else if (c >= 'a' && c <= 'f')
                {
                    b = b * 16 + c + 10 - 'a';
                    v |= (unsigned int)b << bit_mask;
                    if (bit_mask == 0)
                    {
                        output[position++] = v;
                        v = 0;
                        bit_mask = 24;
                    }
                    else
                    {
                        bit_mask -= 8;
                    }
                }
                else
                {
                    throw runtime_error(string("Unrecognized character ") + string(1, c) + " at position " + to_string(i));
                }
                step = 0;
                break;
        }
    }
}

void hex2bin_reverse(string& input, size_t start, size_t length, vector<unsigned char>& output, size_t position)
{
    position += (length / 2 - 1);
    unsigned char b = 0;
    auto step = 0;
    for (size_t i = start; i < start + length; i++)
    {
        auto c = input[i];
        switch (step)
        {
            case 0:
                if (c >= '0' && c <= '9')
                {
                    b = c - '0';
                }
                else if (c >= 'A' && c <= 'F')
                {
                    b = c + 10 - 'A';
                }
                else if (c >= 'a' && c <= 'f')
                {
                    b = c + 10 - 'a';
                }
                else
                {
                    throw runtime_error(string("Unrecognized character ") + string(1, c) + " at position " + to_string(i));
                }
                step = 1;
                break;
            case 1:
                if (c >= '0' && c <= '9')
                {
                    b = b * 16 + c - '0';
                    output[position] = b;
                    position--;
                }
                else if (c >= 'A' && c <= 'F')
                {
                    b = b * 16 + c + 10 - 'A';
                    output[position] = b;
                    position--;
                }
                else if (c >= 'a' && c <= 'f')
                {
                    b = b * 16 + c + 10 - 'a';
                    output[position] = b;
                    position--;
                }
                else
                {
                    throw runtime_error(string("Unrecognized character ") + string(1, c) + " at position " + to_string(i));
                }
                step = 0;
                break;
        }
    }
}

string bin2hex(vector<unsigned char> input, size_t start, size_t length)
{
    string output;
    for (size_t i = start; i < start + length; i++)
    {
        auto b = input[i];
        auto high = b / 16;
        if (high >= 10)
        {
            output.append(1, (char)(high - 10 + 'a'));
        }
        else
        {
            output.append(1, (char)(high + '0'));
        }
        auto low = b % 16;
        if (low >= 10)
        {
            output.append(1, (char)(low - 10 + 'a'));
        }
        else
        {
            output.append(1, (char)(low + '0'));
        }
    }
    return output;
}

string bin2hex_reverse(vector<unsigned char> input, size_t start, size_t length)
{
    string output;
    for (size_t i = start; i < start + length; i++)
    {
        auto b = input[i];
        auto low = b % 16;
        if (low >= 10)
        {
            output.insert(0, 1, (char)(low - 10 + 'a'));
        }
        else
        {
            output.insert(0, 1, (char)(low + '0'));
        }
        auto high = b / 16;
        if (high >= 10)
        {
            output.insert(0, 1, (char)(high - 10 + 'a'));
        }
        else
        {
            output.insert(0, 1, (char)(high + '0'));
        }
    }
    return output;
}

