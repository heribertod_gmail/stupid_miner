#pragma once

enum json_instance_type
{
    json_string,
    json_numeric,
    json_bool,
    json_object,
    json_array,
    json_null
};
