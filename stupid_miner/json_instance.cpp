#include "json_instance.h"

void json_instance::clear()
{
    type = json_null;
    string_value = "";
    numeric_value = 0;
    bool_value = false;
    object_value.clear();
    array_value.clear();
}

size_t json_instance::parse(const std::vector<unsigned char>& source, size_t start)
{
    json_instance instance;
    size_t next_position = 0;
    std::string key;
    std::string input_as_string;
    auto input_as_number = 0.0;
    auto input_is_negative = false;
    auto step = 0;
    auto position = start;
    auto c = source[position];
    while (position < source.size() && step >= 0)
    {
        auto read_next = true;
        switch (step)
        {
            case 0:
                if (c == '{')
                {
                    step = 1;
                }
                else if (c == '[')
                {
                    step = 2;
                }
                else if (c == '"')
                {
                    input_as_string = "";
                    step = 3;
                }
                else if (c == '\'')
                {
                    input_as_string = "";
                    step = 4;
                }
                else if (c >= '0' && c <= '9')
                {
                    input_as_number = 0;
                    input_is_negative = false;
                    step = 5;
                    read_next = false;
                }
                else if (c == '-')
                {
                    input_as_number = 0;
                    input_is_negative = true;
                    step = 5;
                }
                else if (c == 't')
                {
                    step = 6;
                }
                else if (c == 'f')
                {
                    step = 7;
                }
                else if (c == 'n')
                {
                    step = 8;
                }
                else if (c > ' ')
                {
                    throw std::runtime_error(std::string("Unrecognized character ") + std::string(1, c) + " at position " + std::to_string(position));
                }
                break;
            case 1:
                instance.clear();
                next_position = instance.parse(source, position);
                if (instance.type != json_string)
                {
                    throw std::runtime_error(std::string("Expected key name at position ") + std::to_string(position));
                }
                position = next_position;
                key = instance.string_value;
                step = 9;
                break;
            case 2:
                instance.clear();
                position = instance.parse(source, position);
                array_value.push_back(instance);
                step = 12;
                break;
            case 3:
                if (c == '"')
                {
                    string_value = input_as_string;
                    type = json_string;
                    step = -1;
                    read_next = false;
                }
                else
                {
                    input_as_string.append(1, c);
                }
                break;
            case 4:
                if (c == '\'')
                {
                    string_value = input_as_string;
                    type = json_string;
                    step = -1;
                    read_next = false;
                }
                else
                {
                    input_as_string.append(1, c);
                }
                break;
            case 5:
                if (c >= '0' && c <= '9')
                {
                    input_as_number = input_as_number * 10 + (c - '0');
                }
                else
                {
                    if (input_is_negative)
                    {
                        input_as_number = -input_as_number;
                    }
                    numeric_value = input_as_number;
                    type = json_numeric;
                    position--;
                    step = -1;
                    read_next = false;
                }
                break;
            case 6:
                if (c == 'r')
                {
                    step = 13;
                }
                else
                {
                    throw std::runtime_error(std::string("Unrecognized character ") + std::string(1, c) + " at position " + std::to_string(position));
                }
                break;
            case 7:
                if (c == 'a')
                {
                    step = 15;
                }
                else
                {
                    throw std::runtime_error(std::string("Unrecognized character ") + std::string(1, c) + " at position " + std::to_string(position));
                }
                break;
            case 8:
                if (c == 'u')
                {
                    step = 18;
                }
                else
                {
                    throw std::runtime_error(std::string("Unrecognized character ") + std::string(1, c) + " at position " + std::to_string(position));
                }
                break;
            case 9:
                if (c == ':')
                {
                    step = 10;
                }
                else if (c > ' ')
                {
                    throw std::runtime_error(std::string("Unrecognized character ") + std::string(1, c) + " at position " + std::to_string(position));
                }
                break;
            case 10:
                instance.clear();
                position = instance.parse(source, position);
                object_value[key] = instance;
                step = 11;
                break;
            case 11:
                if (c == ',')
                {
                    step = 1;
                }
                else if (c == '}')
                {
                    type = json_object;
                    step = -1;
                    read_next = false;
                }
                else if (c > ' ')
                {
                    throw std::runtime_error(std::string("Unrecognized character ") + std::string(1, c) + " at position " + std::to_string(position));
                }
                break;
            case 12:
                if (c == ',')
                {
                    step = 2;
                }
                else if (c == ']')
                {
                    type = json_array;
                    step = -1;
                    read_next = false;
                }
                else if (c > ' ')
                {
                    throw std::runtime_error(std::string("Unrecognized character ") + std::string(1, c) + " at position " + std::to_string(position));
                }
                break;
            case 13:
                if (c == 'u')
                {
                    step = 14;
                }
                else
                {
                    throw std::runtime_error(std::string("Unrecognized character ") + std::string(1, c) + " at position " + std::to_string(position));
                }
                break;
            case 14:
                if (c == 'e')
                {
                    bool_value = true;
                    type = json_bool;
                    step = -1;
                    read_next = false;
                }
                else
                {
                    throw std::runtime_error(std::string("Unrecognized character ") + std::string(1, c) + " at position " + std::to_string(position));
                }
                break;
            case 15:
                if (c == 'l')
                {
                    step = 16;
                }
                else
                {
                    throw std::runtime_error(std::string("Unrecognized character ") + std::string(1, c) + " at position " + std::to_string(position));
                }
                break;
            case 16:
                if (c == 's')
                {
                    step = 17;
                }
                else
                {
                    throw std::runtime_error(std::string("Unrecognized character ") + std::string(1, c) + " at position " + std::to_string(position));
                }
                break;
            case 17:
                if (c == 'e')
                {
                    type = json_bool;
                    step = -1;
                    read_next = false;
                }
                else
                {
                    throw std::runtime_error(std::string("Unrecognized character ") + std::string(1, c) + " at position " + std::to_string(position));
                }
                break;
            case 18:
                if (c == 'l')
                {
                    step = 19;
                }
                else
                {
                    throw std::runtime_error(std::string("Unrecognized character ") + std::string(1, c) + " at position " + std::to_string(position));
                }
                break;
            case 19:
                if (c == 'l')
                {
                    step = -1;
                    read_next = false;
                }
                else
                {
                    throw std::runtime_error(std::string("Unrecognized character ") + std::string(1, c) + " at position " + std::to_string(position));
                }
                break;
        }
        if (read_next)
        {
            position++;
            if (position < source.size())
            {
                c = source[position];
            }
        }
    }
    return position;
}
