#include "json_instance.h"
#include <sstream>
#include <iostream>
#include <fstream>
#include "hexbin.h"
#include "sha256.h"

using namespace std;

int main(int argc, const char * argv[])
{
    try
    {
        string extra_nonce_1;
        size_t extra_nonce_2_size = 0;
        size_t difficulty = 0;
        string job_file_name;
        string input_file_name;
        auto show_help = (argc == 1);
        for (auto i = 1; i < argc; i++)
        {
            string argument(argv[i]);
            if (argument == "-1")
            {
                i++;
                if (i >= argc)
                {
                    throw runtime_error("Extra nonce 1 value not specified");
                }
                extra_nonce_1 = argv[i];
            }
            else if (argument == "-2")
            {
                i++;
                if (i >= argc)
                {
                    throw runtime_error("Extra nonce 2 size not specified");
                }
                stringstream stream(argv[i], ios_base::in);
                stream >> extra_nonce_2_size;
                if (stream.fail())
                {
                    throw runtime_error("Extra nonce 2 size " + string(argv[i]) + " is not a positive number");
                }
            }
            else if (argument == "-d")
            {
                i++;
                if (i >= argc)
                {
                    throw runtime_error("Difficulty value not specified");
                }
                stringstream stream(argv[i], ios_base::in);
                stream >> difficulty;
                if (stream.fail())
                {
                    throw runtime_error("Difficulty " + string(argv[i]) + " is not a positive number");
                }
            }
            else if (argument == "-j")
            {
                i++;
                if (i >= argc)
                {
                    throw runtime_error("Job file name not specified");
                }
                job_file_name = argv[i];
            }
            else if (argument == "-i")
            {
                i++;
                if (i >= argc)
                {
                    throw runtime_error("Input file name not specified");
                }
                input_file_name = argv[i];
            }
            else if (argument == "-h")
            {
                show_help = true;
            }
            else
            {
                throw runtime_error("Argument " + argument + " not recognized");
            }
        }
        if (show_help)
        {
            cout << "Usage: stupid_miner [arguments]" << endl;
            cout << "Arguments:" << endl;
            cout << " -1 : Extra nonce 1 value, as returned by the result of mining.subscribe" << endl;
            cout << " -2 : Size in bytes of extra nonce 2, as returned by the result of mining.subscribe" << endl;
            cout << " -d : Difficulty for the input job, as returned by mining.set_difficulty" << endl;
            cout << " -j : Path to job file, containing the JSON from mining.notify" << endl;
            cout << " -i : Path to input file where the nonce values will be read from" << endl;
            return EXIT_FAILURE;
        }
        if (extra_nonce_2_size == 0)
        {
            throw runtime_error("Extra nonce 2 size " + to_string(extra_nonce_2_size) + " is not a positive number.");
        }
        cout << "Opening " << job_file_name << endl;
        ifstream job_file(job_file_name, ios_base::binary);
        job_file.seekg(0, ios_base::end);
        size_t job_file_size = job_file.tellg();
        if (job_file_size == 0)
        {
            throw runtime_error(job_file_name + " - empty or absent job file");
        }
        job_file.seekg(0);
        vector<unsigned char> job_source(job_file_size);
        cout << "Reading " << job_file_name << endl;
        job_file.read((char*)(job_source.data()), job_file_size);
        if (job_file.fail())
        {
            throw runtime_error("Could not read data from " + input_file_name);
        }
        job_file.close();
        cout << "Parsing " << job_file_name << endl;
        json_instance job;
        job.parse(job_source, 0);
        cout << "Opening " << input_file_name << endl;
        ifstream input_file(input_file_name, ios_base::binary);
        input_file.seekg(0, ios_base::end);
        size_t input_file_size = input_file.tellg();
        if (input_file_size == 0)
        {
            throw runtime_error(input_file_name + " - empty or absent input file");
        }
        if (extra_nonce_2_size > input_file_size)
        {
            throw runtime_error("Extra nonce 2 size " + to_string(extra_nonce_2_size) + " exceeds the size of the input file.");
        }
        input_file.seekg(0);
        vector<unsigned char> input_source(input_file_size);
        cout << "Reading " << input_file_name << endl;
        input_file.read((char*)(input_source.data()), input_file_size);
        if (input_file.fail())
        {
            throw runtime_error("Could not read data from " + input_file_name);
        }
        input_file.close();
        string& coin_base_1 = job.object_value["params"].array_value[2].string_value;
        string& coin_base_2 = job.object_value["params"].array_value[3].string_value;
        auto coin_base_length = coin_base_1.length() / 2 + extra_nonce_1.length() / 2 + extra_nonce_2_size + coin_base_2.length() / 2;
        vector<unsigned char> coin_base(((coin_base_length + 9) / 64 + 1) * 64);
        hex2bin(coin_base_1, coin_base, 0);
        hex2bin(extra_nonce_1, coin_base, coin_base_1.length() / 2);
        hex2bin(coin_base_2, coin_base, coin_base_length - coin_base_2.length() / 2);
        size_t extra_nonce_2_start = coin_base_1.length() / 2 + extra_nonce_1.length() / 2;
        vector<unsigned char> current_coin_base(coin_base.size());
        vector<unsigned char> nonces(extra_nonce_2_size + 4);
        copy(input_source.begin(), input_source.begin() + extra_nonce_2_size + 4, nonces.begin());
        vector<vector<unsigned int>> merkle_branches;
        for (size_t i = 0; i < job.object_value["params"].array_value[4].array_value.size(); i++)
        {
            merkle_branches.emplace_back(8);
            hex2bin(job.object_value["params"].array_value[4].array_value[i].string_value, merkle_branches[i], 0);
        }
        vector<unsigned int> merkle_root(32);
        vector<unsigned char> block(128);
        hex2bin_reverse(job.object_value["params"].array_value[5].string_value, 0, 8, block, 0);
        size_t block_index = 4;
        for (size_t i = 0; i < 64; i += 8)
        {
            hex2bin_reverse(job.object_value["params"].array_value[1].string_value, i, 8, block, block_index);
            block_index += 4;
        }
        hex2bin_reverse(job.object_value["params"].array_value[7].string_value, 0, 8, block, 68);
        hex2bin_reverse(job.object_value["params"].array_value[6].string_value, 0, 8, block, 72);
        vector<unsigned char> hash(32);
        size_t max_difficulty = (size_t)65535 << (sizeof(size_t) * 8 - 16);
        size_t target_as_number = max_difficulty / difficulty;
        vector<unsigned char> target(32);
        size_t target_shift = sizeof(size_t) * 8 - 8;
        size_t target_bit_mask = (size_t)255 << target_shift;
        for (size_t i = 4; i < 32; i++)
        {
            auto b = (target_as_number & target_bit_mask) >> target_shift;
            target[i] = b;
            target_bit_mask >>= 8;
            if (target_bit_mask == 0)
            {
                break;
            }
            target_shift -= 8;
        }
        size_t position = nonces.size();
        size_t bit_position = 0;
        unsigned char bit_mask = 128;
        auto start_time = chrono::high_resolution_clock::now();
        do
        {
            copy(coin_base.begin(), coin_base.end(), current_coin_base.begin());
            copy(nonces.begin(), nonces.begin() + extra_nonce_2_size, current_coin_base.begin() + extra_nonce_2_start);
            SHA256_Twice(current_coin_base, coin_base_length, merkle_root);
            for (size_t i = 0; i < merkle_branches.size(); i++)
            {
                copy(merkle_branches[i].begin(), merkle_branches[i].end(), merkle_root.begin() + 8);
                SHA256_Twice(merkle_root, 64, merkle_root);
            }
            block_index = 36;
            for (size_t i = 0; i < 8; i++)
            {
                block[block_index++] = (merkle_root[i] >> 24) & 0xFF;
                block[block_index++] = (merkle_root[i] >> 16) & 0xFF;
                block[block_index++] = (merkle_root[i] >> 8) & 0xFF;
                block[block_index++] = merkle_root[i] & 0xFF;
            }
            copy(nonces.begin() + extra_nonce_2_size, nonces.end(), block.begin() + 76);
            SHA256_Twice(block, 80, hash);
            auto found = false;
            size_t hash_index = 31;
            size_t target_index = 0;
            while (target_index < 32)
            {
                if (hash[hash_index] > target[target_index])
                {
                    break;
                }
                else if (hash[hash_index] < target[target_index])
                {
                    found = true;
                    break;
                }
                hash_index--;
                target_index++;
            }
            if (found)
            {
                cout << "Bit position: " << to_string(bit_position) << endl;
                cout << "Byte position: " << to_string(position - nonces.size()) << endl;
                cout << "Hash:   " << bin2hex_reverse(hash, 0, 32) << endl;
                cout << "Target: " << bin2hex(target, 0, 32) << endl;
                cout << "{\"id\": 4, \"method\": \"mining.submit\", \"params\": [\"\", \"" << job.object_value["params"].array_value[0].string_value << "\", \"" << bin2hex(nonces, 0, extra_nonce_2_size) << "\", \"" << job.object_value["params"].array_value[7].string_value << "\", \"" << bin2hex_reverse(nonces, extra_nonce_2_size, 4) << "\"]}" << endl;
                break;
            }
            bit_position++;
            bit_mask >>= 1;
            if (bit_mask == 0)
            {
                position++;
                bit_mask = 128;
            }
            if (position <= input_source.size())
            {
                auto carry = ((input_source[position] & bit_mask) != 0);
                for (size_t i = nonces.size() - 1; i > 0; i--)
                {
                    auto c = nonces[i];
                    auto new_c = c << 1;
                    if (carry)
                    {
                        new_c++;
                    }
                    nonces[i] = new_c;
                    carry = (c > 127);
                }
                auto c = nonces[0];
                c <<= 1;
                if (carry)
                {
                    c++;
                }
                nonces[0] = c;
            }
        } while (position <= input_source.size());
        auto finish_time = chrono::high_resolution_clock::now();
        chrono::duration<double> elapsed = finish_time - start_time;
        cout << "Processing time: " << elapsed.count() << " seconds" << endl;
        return EXIT_SUCCESS;
    }
    catch (exception& e)
    {
        cout << "Error: " << e.what() << endl;
        return EXIT_FAILURE;
    }
}
