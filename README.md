# stupid_miner

The stupidest (yet, fully functional) Bitcoin mining engine ever devised.

... that is, if you're willing to exclude the network connectivity part of it. If you don't, then this is just half a Bitcoin mining engine.

This command-line application will perform networkless Bitcoin mining based on the arguments you specify to it, all of them expected to come from a mining pool conformant with the Stratum protocol - save for one particular exception.

That exception is an input file. This can be any file you wish, as long as the file is long enough to cover the size of the extra nonce 2, plus 4 bytes for the regular nonce. This is because stupid_miner does not perform an incremental search for nonces - instead, it takes them straight from the actual bits of the input file, scanning it from the first (xnonce2_size * 8 + 32) bits, all the way to its last (xnonce2_size * 8 + 32) bits.

Usage: stupid_miner [arguments]

Arguments:

 -1 : Extra nonce 1 value, as returned by the result of mining.subscribe
 
 -2 : Size in bytes of extra nonce 2, as returned by the result of mining.subscribe
 
 -d : Difficulty for the input job, as returned by mining.set_difficulty
 
 -j : Path to job file, containing the JSON from mining.notify
 
 -i : Path to input file where the nonce values will be read from


Since Stratum is a rather simple TCP-based protocol, it is actually easy to connect to a pool (shared or solo) by using a program like telnet (or, less recommended, curl using a telnet:// protocol) to subscribe, authorize and then read jobs that you will then save to a file (one job per file) to send to stupid_miner.

If the odds are in your favor and, for some unexplained reason, stupid_miner gets to find a block, it will output the result straight into the console as a JSON containing a mining.submit method call with all required data - except for the first parameter (the username), and then it will stop. It is up to you to fill in the gaps of the returned JSON, and to send it through the recently opened telnet (or curl) session to submit it to the pool.

Notice that stupid_miner will only read the extra nonce 2 and the regular nonce values from the input file to perform the mining process. Other data, such as the ntime field, will be left intact from the input job file.

And, please, understand: the name of the program is stupid_miner for a reason. It *could* work (it was designed to do so, if the stars align properly and Lady Luck smiles at you at the right moment). But, generally speaking, performing Bitcoin mining using random data from a file should never, ever, EVER work, ever. Don't get your hopes up anytime soon. If anything, stupid_miner should be able to help you understand how Bitcoin mining actually works, in a world where documentation is scarce, poorly written, and actual performant implementations are burned straight into chipsets, thus becoming impossible to learn anything from them.

Source code is standard C++. Packaged inside an Xcode console app project; however, it can be compiled for any platform that supports minimum, basic C++11 features. No external libraries are required; a homemade JSON parser is included, and the code for the SHA-256 hashing algorithm was blatantly stolen from the source code of OpenSSL v3 (available at https://github.com/openssl/openssl ).

Which, by the way, means stupid_miner is licensed under the Apache License 2.0, which means that you are free to get and use it for commercial and non-commercial purposes as long as you fulfill its conditions.

And for those in the know:

    3GMnXQRLE9viJuahG3xxVQQfSH85KS2YmN.0

Thanks!
